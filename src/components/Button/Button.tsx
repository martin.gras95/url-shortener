const Button = (props: {onClick: () => void}) => {
  return (
    <button onClick={props.onClick}>Shorten</button>
  )
}

export default Button