const Input = (props: {handleChange: (value:string) => void}) => {
  return (
    <input type="text" onChange={(e) => props.handleChange(e.target.value)} />
  )
}

export default Input