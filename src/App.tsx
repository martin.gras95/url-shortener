import { useState } from 'react';
import Button from "./components/Button/Button"
import Input from "./components/Input/Input"
import Title from './components/Title/Title';
import { API_URL } from './utils/constants';

function App() {
  const [input, setInput] = useState('')
  const [shortUrl, setShortUrl] = useState('')

  const handleChange = (value: string) => {
    setInput(value)
  }

  async function handleSubmit(input:string){
    try {
      const response = await fetch(API_URL + input);
      if(response.ok){
        const json = await response.json();
        setShortUrl(json.result.full_short_link3);
      }
    } catch (error) {
      throw error;
    }
  }

  return (
    <div className="app" >
      <Title />
      <Input handleChange={(v) => handleChange(v)} />
      <Button onClick={() => handleSubmit(input)} />
      <h2>{shortUrl}</h2>
    </div >
  )
}

export default App
